#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

double a;
double b;
double c;

double scale(double x) {
	return (100*M_1_PI)*asin((x-50)/50)+50;
}

int rng(){
	srand(time(NULL));
	int range = 101;
	//gets us (RAND_MAX + 1) / range, while avoding overflow
	int d = (RAND_MAX - range + 1)/range + 1;
	int r;
	//run rand a few times to get better results on windows
	r = rand();
	r = rand();
	r = rand();
	r = rand();
	r = rand();
	do{
		r = rand();
	}while(r >= (d*range));
	
	return (r%range);

}

void game(char str[]){
	double low = 20;
	double med = 25;
	double high = 30;
	// no easy switch statements for strings :)
	if(strcmp(str,"fn") == 0 || strcmp(str,"fortnite") == 0) {
		low = 2.286228611;
		med = 29.39436786;
		high = 137.1737167;
	}else if(strcmp(str,"yuki") == 0 || strcmp(str,"yukiaim") == 0){
		low = 8.6591;
		med = 27.7091;
		high = 76.9697;
	}else if(strcmp(str,"kovaaks") == 0 || strcmp(str,"aimbeast") == 0){
		low = 12.86;
		med = 24.7403;
		high = 52.0066;
	}else if(strcmp(str,"csgo") == 0 || strcmp(str,"counterstrike") == 0){
		low = 10.9;
		med = 47.2;
		high = 108.2;
	}else if(strcmp(str,"quake") == 0 || strcmp(str,"qc") == 0){
		low = 7.7;
		med = 27.78;
		high = 83.13;
	}else if(strcmp(str,"overwatch") == 0 || strcmp(str,"ow") == 0){
		low = 5.68;
		med = 31.49;
		high = 71.86;
	}else if(strcmp(str,"apex") == 0 || strcmp(str,"apexlegends") == 0){
		low = 11.88;
		med = 34.64;
		high = 74.22;
	}else{
		printf("GAME NOT FOUND, did you spell it correctly???\n");
	}
	b = log( (high-med)/(med-low) ) / 50;
	a = (med-low)/(exp(50*b)-1);
	c = low-a;

	return;
}

double cm360(double x) {
	return a*exp(b*x)+c;
}

int main(int argc,char* argv[]) {
	if(argc==1){
		printf("More arguements required; (game)\n");
		return 0;
	}else if(argc>=2){
		double num;
		//2nd arguement is used for debugging. 0 = minimum sens, 100 = max sens
		// can you guess what 50 is???
		if(argc==2){
			//random value
			num = (double)rng();
		}else{
			//set our value
			num = strtof(argv[2],NULL);
		}

		//Scaling is preformed twice
		double mynum = scale(scale((double)num));
		game(argv[1]);
		double output = cm360(mynum);
		printf("%f\n", output);
	}
	return 0;
	
}
