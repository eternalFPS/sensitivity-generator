This tool will generate you a random sensitivity for a game (or topic) of your choice

![example](imgs/1.png)

# Useage

In windows, download the [.exe](https://gitlab.com/eternalFPS/sensitivity-generator/blob/master/rngcm360.exe) file.  
save it somewhere, and open up either cmd or powershell in the same folder  
then you can run the command with ".\rngcm360.exe \*game\*"  
where you replace \*game\* by the game/topic of your choice  

make sure you type it exactly as it appears below

# Supported Games

* fn / fortnite
* yuki / yukiaim
* kovaaks / aimbest
* csgo / counterstrike
* quake / qc
* overwatch / ow
* apex / apexlegends

# yea but how does it actually work why should i trust it

It will tend towards a bell curve distribution, with the median the exact same, and frequency the exact same as actual pro sensitivities.  
Look at the graph if you want to see how similar this generator is to actual pro sensitivities for CSGO.  

![comparison](imgs/2.png)
